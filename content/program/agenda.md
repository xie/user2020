## Draft Agenda
*Times are currently estimates and are subject to change*

### Monday
- **Other Potential Developer Day Events**
- Geospatial Developer Day (Geosaurus at T-Rex)

### Tuesday
- Diversity Scholar Orientation (8am)
- Tutorials (AM and PM sessions)
- Get Involved with R Core Information Session (during lunch)
- Opening Reception (beginning at 6pm)

### Wednesday
- Mentoring Meet-up (8am)
- Conference Opening and First Keynote (8:45am)
- Diversity Scholar Networking Event (5pm)
- Gala Dinner (6pm)
- City Museum (8pm-11pm)

### Thursday
- First Session begins at 8:30am
- Poster Session (5pm to 6pm)
- Job Fair (6pm to 8pm)
- R-Ladies Reception (7pm to 8:30pm, with presentation at 7:15pm)
- Geospatial Reception (7:30pm to 9pm, with presentation at 8pm)

### Friday
- First Session begins at 8:30am
- R-Core Tutorial (11:30am)
- Conference Closing (ends at 4:30pm)
